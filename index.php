<!DOCTYPE html>
<html lang="en">
<head>
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1">
	<title>Say It!</title>
	<link href="css/sayit.css" rel="stylesheet">
   <link href='http://fonts.googleapis.com/css?family=Dosis' rel='stylesheet' type='text/css'>
   <link href='http://fonts.googleapis.com/css?family=Asul' rel='stylesheet' type='text/css'>
	<script src="https://code.jquery.com/jquery-3.4.1.min.js"
		integrity="sha256-CSXorXvZcTkaix6Yvo6HppcZGetbYMGWSFlBw8HfCJo="
		crossorigin="anonymous"></script>
	<script src="js/sayit.js" defer></script>
</head>
<body>
	<h1>Say It!&trade;</h1>
	<div class="grid_6">
		<h2>What's Been Said ...</h2>
		<div id="beensaid">
					<section>
						<span class="ts">07 Jan 2020 12:54</span>
						<span class="topic">Fun Stuff</span>
						<span class="who">Wize1 says</span>
						I'm having a great time.
					</section>
					<section>
						<span class="ts">07 Jan 2020 12:54</span>
						<span class="topic">Cool Stuff</span>
						Life is good.
					</section>
		</div>
		<button id="update">Update!</button>
	</div>

	<div class="grid_6">
		<h2>Say It Yourself ...</h2>
		<div id="sayit">
			<form method="post" action="./index.php">
				<label>Topic:</label>
				<select name="existing-topic">
					<option>Fun Stuff</option>\n";
					<option>Cool Stuff</option>\n";
				</select>
				or
				<input type="text" name="new-topic" value=""/>
				<div class="error">Topic names are limited to 100 characters</div>
				<div class="clear"></div>
				<div class="error">Post Failed: Message cannot be blank and is limited to 500 characters</div>
				<div class="clear"></div>
				<label>Message (limit 500 chars)</label><br/>
				<textarea name="message"></textarea>
				<button>Say It!</button>
			</form>
		</div>
	</div>
</body>
</html>


